-- -*- coding: utf-8 -*-

--[[

   Copyright 2020 Stephan Hennig

   This file is part of Padrinoma.

   Padrinoma is free software: you can redistribute it and/or modify it
   under the terms of the GNU Affero General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Padrinoma is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public
   License along with Padrinoma.  If not, see
   <http://www.gnu.org/licenses/>.

   Diese Datei ist Teil von Padrinoma.

   Padrinoma ist Freie Software: Sie können es unter den Bedingungen der
   GNU Affero General Public License, wie von der Free Software
   Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder
   späteren veröffentlichten Version, weiterverbreiten und/oder
   modifizieren.

   Padrinoma wird in der Hoffnung, dass es nützlich sein wird, aber OHNE
   JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
   Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN
   ZWECK.  Siehe die GNU Affero General Public License für weitere
   Details.

   Sie sollten eine Kopie der GNU Affero General Public License zusammen
   mit diesem Programm erhalten haben. Wenn nicht, siehe
   <http://www.gnu.org/licenses/>.

--]]



--- This module provides means to test class interfaces.
--
-- @class module
-- @name mod_cls_test
-- @author Stephan Hennig
-- @copyright 2020 Stephan Hennig

-- API-Dokumentation can be generated via <pre>
--
--   luadoc -d API *.lua
--
-- </pre>



-- @trick Prevent LuaDoc from looking past here for module description.
--[[ Trick LuaDoc into entering 'module' mode without using that command.
module(...)
--]]
-- Local module table.
local M = {}



-- Load unicode module.
local unicode = require('unicode')



--- Run all tests of a test collection.
-- A test collection is a table of functions that are executed in
-- increasing order of their keys (indices).  All keys must be of type
-- number, but the table need not necessarily be a sequence.  The result
-- of a test function is never checked.  A failing test has to generate
-- an error.
--
-- @param tests  A test collection (a table of functions).
local function run_tests(tests)
   -- Because tests may build upon each other, we want to make sure to
   -- execute a test with a certain index in the given argument table
   -- before any tests with a higher index, even if the table is not a
   -- sequence.  To manage that, we need to sort the available indices,
   -- first.
   local indices = {}
   for i,_ in pairs(tests) do
      assert(type(i) == 'number', 'Bad test index')
      table.insert(indices, i)
   end
   -- Sort indices.
   table.sort(indices)
   -- Execute tests sorted by index.
   for _,i in ipairs(indices) do
      tests[i]()
   end
end
M.run_tests = run_tests



--- Test instance interface.
-- Check if an instance's existing, non-inherited, non-metatable backed
-- interface is equal to an interface template (a table containing the
-- expected members).
--
-- @param o  An object to test.
-- @param t  Interface template.
-- @return If this function returns, the interfaces match.  In case of
-- failure, this function errors out.
local function test_instance_interface(o, t)
   -- Check object type.
   local type_o = type(o)
   assert(type_o == "table", 'Object expected, got ' .. type_o)
   -- Check instance contains no __pairs meta-method that could
   -- interfere with the following tests.  (An instance always has a
   -- meta-table.)
   assert(getmetatable(o)._pairs == nil, 'Instance has a _pairs metatable')
   -- Check if all keys from interface template are available in the
   -- instance.
   for k,_ in pairs(t) do
      assert(rawget(o, k), 'Missing instance member ' .. tostring(k))
   end
   -- Check if all keys from the instance are available in interface
   -- template.
   for k,_ in pairs(o) do
      assert(t[k], 'Unexpected instance member ' .. tostring(k))
   end
end
M.test_instance_interface = test_instance_interface



--- Test proto-type interface.
-- Check if a proto-type's (an instance's parent) existing interface is
-- equal to an interface template (a table containing the expected
-- members).
--
-- @param o  An object to test.
-- @param t  Interface template.
-- @return If this function returns, the interfaces match.  In case of
-- failure, this function errors out.
local function test_prototype_interface(o, t)
   -- Check object type.
   local type_o = type(o)
   assert(type_o == "table", 'Object expected, got ' .. type_o)
   -- Get reference to proto-type class.
   local p = getmetatable(o).__index
   -- Check proto-type class contains no __pairs meta-method that could
   -- interfere with the following tests.  (Meta-table can only be nil
   -- for the OOP base class.)
   local mt_p = getmetatable(p)
   assert(mt_p == nil or mt_p._pairs == nil, 'Proto-type has a _pairs metatable')
   -- Check if all keys from interface template are available in the
   -- proto-type class.
   for k,_ in pairs(t) do
      assert(rawget(p, k), 'Missing proto-type member ' .. tostring(k))
   end
   -- Check if all keys from the proto-type class are available in the
   -- interface template.  Make an exception for __index, which is not
   -- part of the interface, but serves class inheritance.
   for k,_ in pairs(p) do
      assert(t[k] or k == "__index", 'Unexpected proto-type member ' .. tostring(k))
   end
end
M.test_prototype_interface = test_prototype_interface



--- Search a file in the texmf tree.
--
-- @param name  File name.
-- @return Path to file.
local function search_texmf(name)
   -- Call kpsewhich.
   local pipe = assert(io.popen('kpsewhich ' .. name, 'r'))
   -- Read path from first line in pipe.
   local path = pipe:read('*l')
   pipe:close()
   assert(path ~= "", 'File not found')
--   io.write('Found file ', path, '\n')
   return path
end
M.search_texmf = search_texmf



--- Read TeX patterns from Knuth's pattern file hyphen.tex.
-- Because these patterns are frozen, they are good test input.
--
-- @param o  A trie or pattern object.
local function read_hyphen_tex(o)
   -- Search path of Knuth pattern file.
   local fname = search_texmf('hyphen.tex')
   -- Read file at once.
   local fin = assert(io.open(fname, 'r'))
   local all_patterns = fin:read('*a')
   fin:close()
   -- Since hyphen.tex contains more than just patterns, filter for
   -- patterns.  Remove all comments.  Then match \patterns{} argument.
   all_patterns = unicode.utf8.match(unicode.utf8.gsub(all_patterns, '%%.-[\n\r]+', ' '), '\\patterns{(.-)}')
   assert(all_patterns, 'Failure reading file ' .. fname)
   -- Retrieve reference to trie object.
   local trie = o.trie or o
   assert(trie:get_root(), 'Invalid trie object')
   -- Iterate over patterns.
   local cnt_patterns = 0
   for pattern in unicode.utf8.gmatch(all_patterns, '%S+') do
      -- Insert pattern.
      trie:insert_record(pattern)
      cnt_patterns = cnt_patterns + 1
   end
--   print(cnt_patterns)
   assert(cnt_patterns == 4447, 'Unexpected number of patterns read')
end
M.read_hyphen_tex = read_hyphen_tex



-- Return module table.
return M
