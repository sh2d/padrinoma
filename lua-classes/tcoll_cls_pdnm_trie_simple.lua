-- -*- coding: utf-8 -*-



-- Load test module.
local mtest = require('mod_class_test')
-- Load class module to test.
local class = require('cls_pdnm_trie_simple')
-- Load unicode module.
local unicode = require('unicode')



-- A sequence of all tests.  Test are provided as functions.
local TC = {



   --- Test class interface.
   -- A class instance is expected to inherit exactly the following
   -- (raw) interface from its proto-type class.
   [100] = function ()
      local o = class:new()
      mtest.test_prototype_interface(
         o,
         {
            --
            -- Private members.
            --
            -- Initialized in OOP base class.
            super = true,
            init = true,
            -- Declared in proto-type module.
            file_chunks = true,
            file_records = true,
            --
            -- Public members.
            --
            -- Declared in proto-type module.
            get_root = true,
            new_node = true,
            set_value = true,
            get_value = true,
            key = true,
            insert = true,
            find = true,
            bfs = true,
            dfs = true,
            set_buffer_size = true,
            get_buffer_size = true,
            record_to_value = true,
            record_to_key = true,
            insert_record = true,
            read_file = true,
            debug_bfs = true,
            debug_dfs = true,
            count_nodes = true,
            value_to_string = true,
            debug_output_visit_full = true,
            debug_output_visit_sparse = true,
            debug_output = true,
            debug_dot_nname = true,
            debug_dot_visit_output_node = true,
            debug_dot_level_preamble_output_node = true,
            debug_dot_level_postamble_output_node = true,
            debug_dot_visit_output_transition_edges = true,
            debug_dot = true,
         }
      )
   end,



   --- Test class interface.
   -- A class instance is expected to provide exactly the following
   -- (raw) interface.
   [200] = function ()
      local o = class:new()
      mtest.test_instance_interface(
         o,
         {
            --
            -- Private members.
            --
            -- Initialized in OOP base class constructor.
            super = true,
            -- Initialized in proto-type constructor.
            file_buffer_size = true,
            root = true,
            --
            -- Public members.
            --
            value = true,
         }
      )
   end,



   --- A trie object can return its root.
   [300] = function ()
      local o = class:new()
      local r = o:get_root()
      assert(type(r) == 'table', 'Bad root node')
   end,



   --- Test low-level interface.
   -- A trie can create a new node, insert the node at the root, store a
   -- value for that node and retrieve that value.
   [400] = function ()
      local o = class:new()
      -- One-letter key and associated value.
      local letter = 'a'
      local value = 1234
      -- Retrieve root node.
      local root = o:get_root()
      -- Create a new node.
      local node = o:new_node()
      assert(type(node) == 'table', 'Bad new node type')
      -- Link node to root.
      root[letter] = node
      -- Store value.
      o:set_value(node, value)
      -- Retrieve value.
      local v = o:get_value(node)
      assert(v == value, 'Bad value retrieved')
      -- Try to retrieve value via high-level interface, too.
      local v = o:find({ letter })
      assert(v ~= nil, 'Illegal letter')
      assert(v == value, 'Bad value retrieved')
   end,



   --- A Lua value can be turned into table representation suitable for
   --- insertion into a trie.
   [500] = function ()
      local o = class:new()
      -- A string value is converted into a table containing a sequence
      -- of its characters.
      local s = 'test'
      local key = o:key(s)
      assert(type(key) == 'table', 'Bad key type')
      assert(#key == unicode.utf8.len(s) and table.concat(key) == s, 'Key mismatch')
      -- A table value is returned as is.
      local s = { 'test', 33 }
      local key = o:key(s)
      assert(key == s, 'Key mismatch')
      -- Any other value is returned as a table containing the value as
      -- the only item.
      local s = 207
      local key = o:key(s)
      assert(type(key) == 'table', 'Bad key type')
      assert(#key == 1 and key[1] == s, 'Key mismatch')
      local s = true
      local key = o:key(s)
      assert(type(key) == 'table', 'Bad key type')
      assert(#key == 1 and key[1] == s, 'Key mismatch')
      local s = function(a,b) return a<b end
      local key = o:key(s)
      assert(type(key) == 'table', 'Bad key type')
      assert(#key == 1 and key[1] == s, 'Key mismatch')
   end,



   --- Test high-level interface.
   -- A key can be inserted into a trie and retrieved again.
   [600] = function ()
      local o = class:new()
      local key = o:key('test')
      local value = 1234
      -- Insert key with associated value.
      o:insert(key, value)
      -- Retrieve value.
      local v = o:find(key)
      assert(v ~= nil, 'Could not find key')
      assert(v == value, 'Bad value returned')
   end,



   --- Keys can be inserted multiple times.
   -- After inserting a key twice with different associated values, a
   -- search for that key returns the latest value inserted.
   [700] = function ()
      local o = class:new()
      local key = o:key('test')
      local v1 = 1234
      local v2 = { true, false, true }
      -- Insert key twice.
      o:insert(key, v1)
      o:insert(key, v2)
      -- Retrieve value.
      local v = o:find(key)
      assert(v ~= nil, 'Could not find key')
      assert(v == v2, 'Key returnd with bad value')
   end,



   --- String characters are assumed to be in the UTF-8 encoding.
   -- Check multi-byte characters are treated as one character.  Assert
   -- on the number of nodes resulting from known input.
   [800] = function ()
      local o = class:new()
      local n = 1
      -- Some German and French accented letters.
      local key = 'ÄÖÜäöüßÀÂÆÇÈÉÊËÎÏÔŒÙÛÜŸàâæçèéêëîïôœùûüÿ'
      n = n + unicode.utf8.len(key)
      key = o:key(key)
      o:insert(key, true)
      assert(o:count_nodes() == n, 'Bad number of nodes')
      -- Inserting this multi-byte key twice doesn't change node count.
      o:insert(key, true)
      assert(o:count_nodes() == n, 'Bad number of nodes')
      -- A German word.
      local key = 'Fußgängerunterführung'-- pedestrian subway
      n = n + unicode.utf8.len(key)
      key = o:key(key)
      o:insert(key, true)
      assert(o:count_nodes() == n, 'Bad number of nodes')
      -- A French word.
      local key = 'égalité'-- equality
      n = n + unicode.utf8.len(key)
      key = o:key(key)
      o:insert(key, true)
      assert(o:count_nodes() == n, 'Bad number of nodes')
   end,



   --- Buffer size for file reading can be changed.
   [900] = function ()
      local o = class:new()
      local s = o:get_buffer_size()
      assert(type(s) == 'number' and s > 0, 'Bad buffer size')
      local buff = math.floor(s * 1.1)
      o:set_buffer_size(buff)
      local s = o:get_buffer_size()
      assert(s == buff, 'Bad buffer size')
   end,



   --- A record read from a file can be converted into a key and a value
   --- before insertion into a trie.
   -- Test default implementations.
   [1000] = function ()
      local o = class:new()
      local record = 'test'
      -- Default record to key implementation is identical to key
      -- function.
      local key = o:record_to_key(record)
      local k = o:key(record)
      assert(type(key) == 'table', 'Bad key type')
      assert(#key == #k and table.concat(key) == table.concat(k), 'Key mismatch')
      -- Default record to value implementation returns a true value.
      local value = o:record_to_value(record)
      assert(value == true, 'Value mismatch')
   end,



   --- Read keys from a file.
   -- Keys are read from Knuth's TeX pattern file hyphen.tex.  Check
   -- number of nodes in Knuth pattern trie.  In this test, pattern
   -- levels (numbers) are removed before insertion.  By default,
   -- associated value is boolean true.
   [1100] = function ()
      -- Custom key generation.
      local function record_to_key(self, rec)
         assert(type(rec) == 'string', 'Bad record type')
         return self:key(unicode.utf8.gsub(rec, '[0-9]+', ''))
      end
      local o = class:new()
      o.record_to_key = record_to_key
      -- Measuring trie memory consumption commands are commented out.
      -- Last time this measurement was done, storing 4447 Knuth
      -- patterns in a trie claimed 0.88 MB of memory.  With 7112 trie
      -- nodes, average node size was ca. 130 bytes.
      --
--      local kb_before, kb_after = 0, 0
--      collectgarbage('collect')
--      kb_before = collectgarbage('count')
      mtest.read_hyphen_tex(o)
--      collectgarbage('collect')
--      kb_after = collectgarbage('count')
--      local kb = kb_after - kb_before
      local nodes = o:count_nodes()
--      print(string.format('nodes: %d, mem: %f MB, %f bytes/node', nodes, kb / 1024, kb * 1024 / nodes))
      assert(nodes == 7112, 'Bad number of nodes in trie')
   end,



   --- Insert a record into a trie.
   -- The record is transformed into a key-value pair before insertion.
   [1200] = function ()
      local o = class:new()
      local record = 'test'
      -- Insert record.
      local old_value = o:insert_record(record)
      assert(old_value == nil, 'Bad record insertion')
      -- Retrieve record.
      local key = o:key(record)
      local k = o:find(key)
      assert(k, 'Cannot retrieve key')
      -- Insert record twice.
      local old_value = o:insert_record(record)
      assert(old_value == true, 'Bad double record insertion')
   end,



   --- To test DFS recursion, we implement a node counter.
   -- Such a function is already provided by the trie.  But to test DFS
   -- recursion, we build our own here.
   [1300] = function ()
      local n
      local function visit(self, node)
         n = n + 1
      end
      local o = class:new()
      -- Test empty trie.
      n = 0
      o:dfs(visit)
--      print(n)
      assert(n == 1, 'Bad number of nodes')
      -- Populate trie.
      o:insert_record('abcd')
      o:insert_record('abef')
      -- Test populated trie.
      n = 0
      o:dfs(visit)
      assert(n == 7, 'Bad number of nodes')
   end,



   --- To test debug DFS recursion, we implement a node counter (once
   --- again).
   -- Such a function is already provided by the trie.  But to test DFS
   -- recursion, we build our own here.
   [1400] = function ()
      local n
      local function visit(self, node)
         n = n + 1
      end
      local o = class:new()
      -- Test empty trie.
      n = 0
      o:debug_dfs(visit)
--      print(n)
      assert(n == 1, 'Bad number of nodes')
      -- Populate trie.
      o:insert_record('abcd')
      o:insert_record('abef')
      -- Test pre-order traversal.
      n = 0
      o:debug_dfs(visit)
      assert(n == 7, 'Bad number of nodes')
      -- Test post-order traversal.
      n = 0
      o:debug_dfs(nil, visit)
      assert(n == 7, 'Bad number of nodes')
   end,



   --- To test debug BFS recursion, we implement a node counter (once
   --- again).
   -- Such a function is already provided by the trie.  But to test BFS
   -- recursion, we build our own here.
   [1500] = function ()
      local n
      local function visit(self, node)
         n = n + 1
      end
      local o = class:new()
      -- Test empty trie.
      n = 0
      o:debug_bfs(visit)
--      print(n)
      assert(n == 1, 'Bad number of nodes')
      -- Populate trie.
      o:insert_record('abcd')
      o:insert_record('abef')
      -- Test populated traversal.
      n = 0
      o:debug_bfs(visit)
      assert(n == 7, 'Bad number of nodes')
   end,



   --- To test BFS recursion, we implement a node counter (once again).
   -- Such a function is already provided by the trie.  But to test BFS
   -- recursion, we build our own here.
   [1600] = function ()
      local n
      local function visit(self, node)
         n = n + 1
      end
      local o = class:new()
      -- Test empty trie.
      n = 0
      o:bfs(visit)
--      print(n)
      assert(n == 1, 'Bad number of nodes')
      -- Populate trie.
      o:insert_record('abcd')
      o:insert_record('abef')
      -- Test populated traversal.
      n = 0
      o:bfs(visit)
      assert(n == 7, 'Bad number of nodes')
   end,



}



-- Run test collection.
mtest.run_tests(TC)
