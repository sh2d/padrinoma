-- -*- coding: utf-8 -*-



-- Load test module.
local mtest = require('mod_class_test')
-- Load class module to test.
local class = require('cls_pdnm_spot')



-- A sequence of all tests.  Test are provided as functions.
local TC = {



   --- Test class interface.
   -- A class instance is expected to inherit exactly the following
   -- (raw) interface from its proto-type class.
   [100] = function ()
      local o = class:new()
      mtest.test_prototype_interface(
         o,
         {
            --
            -- Private members.
            --
            -- Initialized in OOP base class.
            super = true,
            init = true,
            -- Initialized in proto-type parent constructor.  Shadowed
            -- by instance members (see below).
            trie = true,
            trie_root = true,
            boundary_letter = true,
            --
            -- Public members.
            --
            -- Declared in proto-type module.
            get_spot_chars = true,
            set_spot_chars = true,
            get_spot_mins = true,
            set_spot_mins = true,
            find_levels = true,
            cb_pdnm_pattern__decomposition_start = true,
            cb_pdnm_pattern__decomposition_pre_iterate_active_tries = true,
            cb_pdnm_pattern__decomposition_pattern_found = true,
            cb_pdnm_pattern__decomposition_finish = true,
            to_word_with_levels = true,
            to_word_with_spots = true,
         }
      )
   end,



   --- Test class interface.
   -- A class instance is expected to provide exactly the following
   -- (raw) interface.
   [200] = function ()
      local o = class:new()
      mtest.test_instance_interface(
         o,
         {
            --
            -- Private members.
            --
            super = true,
            -- Initialized in proto-type constructor.
            leading_spot_min = true,
            trailing_spot_min = true,
            spot_char = true,
            explicit_spot_char = true,
            -- Initialized in proto-type parent constructor.
            trie = true,
            trie_root = true,
            boundary_letter = true,
            --
            -- Public members.
            --
         }
      )
   end,



   --- Check setting/getting spot chars.
   [300] = function ()
      local o = class:new()
      -- Check default spot chars.
      local spot_ch, ex_spot_ch = o:get_spot_chars()
      assert(spot_ch == '-' and ex_spot_ch == '=', 'Bad default spot chars')
      -- Check setting individual spot chars.
      local spot_ch, ex_spot_ch = '<', '>'
      o:set_spot_chars(spot_ch, ex_spot_ch)
      local spc, espc = o:get_spot_chars()
      assert(spc == spot_ch and espc == ex_spot_ch, 'Bad spot chars')
   end,



   --- Check setting/getting spot mins.
   [400] = function ()
      local o = class:new()
      -- Check default spot chars.
      local leading_min, trailing_min = o:get_spot_mins()
      assert(leading_min == 2 and trailing_min == 2, 'Bad default spot mins')
      -- Check setting individual spot chars.
      local leading_min, trailing_min = 3, 4
      o:set_spot_mins(leading_min, trailing_min)
      local lmin, tmin = o:get_spot_mins()
      assert(lmin == leading_min and tmin == trailing_min, 'Bad spot mins')
   end,



   --- Read patterns from a file.
   -- Patterns are read from Knuth's TeX pattern file hyphen.tex.  Check
   -- number of nodes in Knuth pattern trie.  By default, associated
   -- value is a table containing pattern levels.
   [500] = function ()
      local o = class:new()
      -- Measuring trie memory consumption commands are commented out.
      -- Last time this measurement was done, storing 4447 Knuth
      -- patterns in a trie claimed 1.3 MB of memory.  With 7112 trie
      -- nodes, average node size was ca. 189 bytes.
      --
--      local kb_before,
--      kb_after = 0, 0 collectgarbage('collect') kb_before =
--      collectgarbage('count')
      mtest.read_hyphen_tex(o)
--      collectgarbage('collect')
--      kb_after = collectgarbage('count')
--      local kb = kb_after - kb_before
      local nodes = o.trie:count_nodes()
--      print(string.format('nodes: %d, mem: %f MB, %f bytes/node', nodes, kb / 1024, kb * 1024 / nodes))
      assert(nodes == 7112, 'Bad number of nodes in trie')
   end,



   -- Check all call-backs are called.
   [600] = function ()
      -- Initialize counters associated to call-backs.
      local cnt_cb = {
         decomposition_start = 0,
         decomposition_pre_iterate_active_tries = 0,
         decomposition_pattern_found = 0,
         decomposition_finish = 0,
      }
      -- Call-backs increase the associated counter.
      local function inc(cnt) cnt_cb[cnt] = cnt_cb[cnt] + 1 end
      local cb = {
         cb_pdnm_pattern__decomposition_start = function (self) inc('decomposition_start') end,
         cb_pdnm_pattern__decomposition_pre_iterate_active_tries = function (self) inc('decomposition_pre_iterate_active_tries') end,
         cb_pdnm_pattern__decomposition_pattern_found = function (self, node, start) inc('decomposition_pattern_found') end,
         cb_pdnm_pattern__decomposition_finish = function (self) inc('decomposition_finish') end,
      }
      local o = class:new()
      mtest.read_hyphen_tex(o)
      -- Hook into call-backs.
      for name,f in pairs(cb) do
         o[name] = f
      end
      -- Determine levels in a word.
      o:find_levels(o:to_word('test'))
      for cb,cnt in pairs(cnt_cb) do
         -- Check call-back calling frequency.
         assert(cnt > 0, 'Bad call-back management')
      end
   end,



   --- Levels in a word can be identified.
   [700] = function ()
      -- Table mapping a test string to a sequence of expected levels.
      local test_strings = {
         ['word'] = { 0, 0, 2, 0, 0 },
         ['hyphenation'] = { 0, 0, 3, 0, 0, 2, 5, 4, 2, 0, 0, 0 },
         ['by'] = { 0, 0, 0 },
         ['computer'] = { 0, 0, 4, 5, 0, 2, 3, 0, 0 },
         ['mississippi'] = { 0, 0, 2, 1, 4, 2, 1, 2, 4, 1, 0, 0 },
         -- Strings containing an explicit hyphen.  Note, applying
         -- patterns that have not been created with explicit hyphens in
         -- mind, to a word containing any such, is most often wrong.
         -- Patterns have to be applied to each word component
         -- separately.  But we know what's going on in this tests.
         ['off-line'] = { 0, 0, 1, 0, 1, 4, 2, 0, 0 },
         ['ultra-ambitious'] = { 0, 0, 1, 0, 0, 3, 0, 4, 3, 2, 3, 2, 0, 2, 0, 0 },
      }
      --
      -- Declare function that does the actual test.
      local function test(o, s, expected)
--         print(s)
         local word = o:to_word(s)
         local found_levels = o:find_levels(word)
         assert(type(found_levels) == 'table', 'Bad result type: find_levels()')
         assert(#found_levels == #expected, 'Bad result table length: find_levels()')
         -- Compare all result table entries.
         for i,level in pairs(found_levels) do
--            print(i, level)
            assert(level == expected[i], 'Bad level in result table: find_levels()')
         end
      end
      --
      local o = class:new()
      mtest.read_hyphen_tex(o)
      -- Run test set through tests.
      for s,expected in pairs(test_strings) do
         test(o, s, expected)
      end
   end,



   --- A word in table format can be intermingled with levels.
   [800] = function ()
      -- Table mapping a test string to a sequence of letters and
      -- expected levels.
      local test_strings = {
         ['word'] = { '.', 0, 'w', 0, 'o', 2, 'r', 0, 'd', 0, '.' },
         ['hyphenation'] = { '.', 0, 'h', 0, 'y', 3, 'p', 0, 'h', 0, 'e', 2, 'n', 5, 'a', 4, 't', 2, 'i', 0, 'o', 0, 'n', 0, '.' },
         ['by'] = { '.', 0, 'b', 0, 'y', 0, '.' },
         ['computer'] = { '.', 0, 'c', 0, 'o', 4, 'm', 5, 'p', 0, 'u', 2, 't', 3, 'e', 0, 'r', 0, '.' },
         ['mississippi'] = { '.', 0, 'm', 0, 'i', 2, 's', 1, 's', 4, 'i', 2, 's', 1, 's', 2, 'i', 4, 'p', 1, 'p', 0, 'i', 0, '.' },
         -- Strings containing an explicit hyphen.  Note, applying
         -- patterns that have not been created with explicit hyphens in
         -- mind, to a word containing any such, is most often wrong.
         -- Patterns have to be applied to each word component
         -- separately.  But we know what's going on in this tests.
         ['off-line'] = { '.', 0, 'o', 0, 'f', 1, 'f', 0, '-', 1, 'l', 4, 'i', 2, 'n', 0, 'e', 0, '.' },
         ['ultra-ambitious'] = { '.', 0, 'u', 0, 'l', 1, 't', 0, 'r', 0, 'a', 3, '-', 0, 'a', 4, 'm', 3, 'b', 2, 'i', 3, 't', 2, 'i', 0, 'o', 2, 'u', 0, 's', 0, '.' }
      }
      --
      -- Declare function that does the actual test.
      local function test(o, s, expected)
--         print(s)
         local word = o:to_word(s)
         local levels = o:find_levels(word)
         local word_with_levels = o:to_word_with_levels(word, levels)
--         print(table.concat(word_with_levels))
         assert(type(word_with_levels) == 'table', 'Bad result type: to_word_with_levels()')
         assert(#word_with_levels == #expected, 'Bad result table length: to_word_with_levels()')
         -- Compare all result table entries.
         for i,item in pairs(word_with_levels) do
--            print(i, item)
            assert(i % 2 == 0 and type(item) == 'number' or type(item) == 'string', 'Bad item type in result table: to_word_with_levels()')
            assert(item == expected[i], 'Bad item in result table: to_word_with_levels()')
         end
      end
      --
      local o = class:new()
      mtest.read_hyphen_tex(o)
      -- Run test set through tests.
      for s,expected in pairs(test_strings) do
         test(o, s, expected)
      end
   end,



   --- A word in table format and levels can be converted to a word (in
   --- table format) containing spot characters.
   [900] = function ()
      local test_strings = {
         ['word'] = { 'w', 'o', 'r', 'd' },
         ['hyphenation'] = { 'h', 'y', '-', 'p', 'h', 'e', 'n', '-', 'a', 't', 'i', 'o', 'n' },
         ['by'] = { 'b', 'y' },
         ['computer'] = { 'c', 'o', 'm', '-', 'p', 'u', 't', '-', 'e', 'r' },
         ['mississippi'] = { 'm', 'i', 's', '-', 's', 'i', 's', '-', 's', 'i', 'p', '-', 'p', 'i' },
         -- Strings containing an explicit hyphen.  Note, applying
         -- patterns that have not been created with explicit hyphens in
         -- mind, to a word containing any such, is most often wrong.
         -- Patterns have to be applied to each word component
         -- separately.  But we know what's going on in this tests.
         ['off-line'] = { 'o', 'f', '-', 'f', '=', '-', 'l', 'i', 'n', 'e' },
         ['ultra-ambitious'] = { 'u', 'l', '-', 't', 'r', 'a', '-', '=', 'a', 'm', '-', 'b', 'i', '-', 't', 'i', 'o', 'u', 's' }
      }
      --
      -- Declare function that does the actual test.
      local function test(o, s, expected)
--         print(s)
         local word = o:to_word(s)
         local levels = o:find_levels(word)
         local word_with_spots = o:to_word_with_spots(word, levels)
--         print(table.concat(word_with_spots))
         assert(type(word_with_spots) == 'table', 'Bad result type: to_word_with_spots()')
         assert(#word_with_spots == #expected, 'Bad result table length: to_word_with_spots()')
         -- Compare all result table entries.
         for i,item in pairs(word_with_spots) do
--            print(i, item)
            assert(item == expected[i], 'Bad item in result table: to_word_with_spots()')
         end
      end
      --
      local o = class:new()
      mtest.read_hyphen_tex(o)
      -- Run test set through tests.
      for s,expected in pairs(test_strings) do
         test(o, s, expected)
      end
   end,



}



-- Run test collection.
mtest.run_tests(TC)
