-- -*- coding: utf-8 -*-



-- Load test module.
local mtest = require('mod_class_test')
-- Load class module to test.
local class = require('cls_pdnm_pattern')



-- A sequence of all tests.  Test are provided as functions.
local TC = {



   --- Test class interface.
   -- A class instance is expected to inherit exactly the following
   -- (raw) interface from its proto-type class.
   [100] = function ()
      local o = class:new()
      mtest.test_prototype_interface(
         o,
         {
            --
            -- Private members.
            --
            -- Initialized in OOP base class.
            super = true,
            init = true,
            --
            -- Public members.
            --
            -- Declared in proto-type module.
            get_boundary_letter = true,
            set_boundary_letter = true,
            to_word = true,
            read_file = true,
            decomposition_start = true,
            decomposition_advance = true,
            decomposition_finish = true,
            decompose = true,
            cb_pdnm_pattern__decomposition_start = true,
            cb_pdnm_pattern__decomposition_pre_iterate_active_tries = true,
            cb_pdnm_pattern__decomposition_pattern_found = true,
            cb_pdnm_pattern__decomposition_finish = true,
         }
      )
   end,



   --- Test class interface.
   -- A class instance is expected to provide exactly the following
   -- (raw) interface.
   [200] = function ()
      local o = class:new()
      mtest.test_instance_interface(
         o,
         {
            --
            -- Private members.
            --
            -- Initialized in OOP base class constructor.
            super = true,
            -- Initialized in proto-type constructor.
            trie = true,
            trie_root = true,
            boundary_letter = true,
            --
            -- Public members.
            --
         }
      )
   end,



   --- Test setting/getting boundary letter.
   [300] = function ()
      local o = class:new()
      -- Default boundary letter is FULL STOP, 0x2e.
      assert(o:get_boundary_letter() == string.char(0x2e), 'Bad default boundary letter')
      -- Try with a multi-byte character..
      local b = 'ß'-- LATIN SMALL LETTER SHARP S, 0xce9f
      o:set_boundary_letter(b)
      -- Access boundary letter directly.
      assert(o.boundary_letter == b, 'Bad boundary letter.')
      -- Access boundary letter via getter.
      assert(o:get_boundary_letter() == b, 'Bad boundary letter.')
   end,



   --- A Lua value can be turned into table representation suitable for
   --- insertion into the pattern trie.
   [400] = function ()
      local o = class:new()
      -- A string value is converted into a table containing a sequence
      -- of its characters.
      local s = 'test'
      local key = o:to_word(s)
      assert(type(key) == 'table', 'Bad key type')
      assert(#key == #s and table.concat(key) == s, 'Key mismatch')
      -- A table value is returned as is.
      local s = { 'test', 33 }
      local key = o:to_word(s)
      assert(key == s, 'Key mismatch')
      -- Any other value is returned as a table containing the value as
      -- the only item.
      local s = 207
      local key = o:to_word(s)
      assert(type(key) == 'table', 'Bad key type')
      assert(#key == 1 and key[1] == s, 'Key mismatch')
      local s = true
      local key = o:to_word(s)
      assert(type(key) == 'table', 'Bad key type')
      assert(#key == 1 and key[1] == s, 'Key mismatch')
      local s = function(a,b) return a<b end
      local key = o:to_word(s)
      assert(type(key) == 'table', 'Bad key type')
      assert(#key == 1 and key[1] == s, 'Key mismatch')
   end,



   --- Read patterns from a file.
   -- Patterns are read from Knuth's TeX pattern file hyphen.tex.  Check
   -- number of nodes in Knuth pattern trie.  By default, associated
   -- value is the full pattern string.
   [500] = function ()
      local o = class:new()
      -- Measuring trie memory consumption commands are commented out.
      -- Last time this measurement was done, storing 4447 Knuth
      -- patterns in a trie claimed 1.1 MB of memory.  With 7112 trie
      -- nodes, average node size was ca. 163 bytes.
      --
--      local kb_before,
--      kb_after = 0, 0 collectgarbage('collect') kb_before =
--      collectgarbage('count')
      mtest.read_hyphen_tex(o)
--      collectgarbage('collect')
--      kb_after = collectgarbage('count')
--      local kb = kb_after - kb_before
      local nodes = o.trie:count_nodes()
--      print(string.format('nodes: %d, mem: %f MB, %f bytes/node', nodes, kb / 1024, kb * 1024 / nodes))
      assert(nodes == 7112, 'Bad number of nodes in trie')
   end,



   -- Check all call-backs are called.
   [600] = function ()
      -- Initialize counters associated to call-backs.
      local cnt_cb = {
         decomposition_start = 0,
         decomposition_pre_iterate_active_tries = 0,
         decomposition_pattern_found = 0,
         decomposition_finish = 0,
      }
      -- Call-backs increase the associated counter.
      local function inc(cnt) cnt_cb[cnt] = cnt_cb[cnt] + 1 end
      local cb = {
         cb_pdnm_pattern__decomposition_start = function (self) inc('decomposition_start') end,
         cb_pdnm_pattern__decomposition_pre_iterate_active_tries = function (self) inc('decomposition_pre_iterate_active_tries') end,
         cb_pdnm_pattern__decomposition_pattern_found = function (self, node, start) inc('decomposition_pattern_found') end,
         cb_pdnm_pattern__decomposition_finish = function (self) inc('decomposition_finish') end,
      }
      local o = class:new()
      mtest.read_hyphen_tex(o)
      -- Hook into call-backs.
      for name,f in pairs(cb) do
         o[name] = f
      end
      -- Decompose a word.
      o:decompose(o:to_word('test'))
      for cb,cnt in pairs(cnt_cb) do
         -- Check call-back calling frequency.
         assert(cnt > 0, 'Bad call-back management')
      end
   end,



   -- Check decomposition result.
   [700] = function ()
      -- Table mapping a test string to a table containing expected
      -- decomposition patterns and number of occurence of that pattern.
      local test_strings = {
         ['word'] = {
            ['1wo2'] = 1,
         },
         ['hyphenation'] = {
            ['hy3ph'] = 1,
            ['he2n'] = 1,
            ['hena4'] = 1,
            ['hen5at'] = 1,
            ['1na'] = 1,
            ['n2at'] = 1,
            ['1tio'] = 1,
            ['2io'] = 1,
            ['o2n'] = 1,
         },
         ['by'] = {
            ['5by.'] = 1,
         },
         ['computer'] = {
            ['1co'] = 1,
            ['4m1p'] = 1,
            ['pu2t'] = 1,
            ['5pute'] = 1,
            ['put3er'] = 1,
         },
         ['mississippi'] = {
            ['.mis1'] = 1,
            ['m2is'] = 1,
            ['4is1s'] = 2,
            ['2ss'] = 2,
            ['s1si'] = 2,
            ['1sis'] = 1,
            ['2ip'] = 1,
            ['4p1p'] = 1,
         },
         -- A pattern object doesn't fold letter-case.  Some patterns
         -- matching at the beginning of a string are therefore missing,
         -- cf. string 'mississippi'.
         ['Mississippi'] = {
            ['4is1s'] = 2,
            ['2ss'] = 2,
            ['s1si'] = 2,
            ['1sis'] = 1,
            ['2ip'] = 1,
            ['4p1p'] = 1,
         },
         -- Strings containing an explicit hyphen.  Note, applying
         -- patterns that have not been created with explicit hyphens in
         -- mind, to a word containing any such, is most often wrong.
         -- Patterns have to be applied to each word component
         -- separately.  But we know what's going on in this tests.
         ['off-line'] = {
            ['4f1f'] = 1,
            ['1l4ine'] = 1,
            ['2ine'] = 1,
            ['2ne.'] = 1,
         },
      }
      --
      -- Declare function that does the actual test.
      local function test(o, s, expected)
--         print(s)
         -- Store reference to table of expected patterns in object.
         o.expected = expected
         o:decompose(o:to_word(s))
         o.expected = nil
         for pattern,cnt in pairs(expected) do
--            print(pattern, cnt)
            -- Detect missing or too frequent patterns.
            assert(cnt == 0, 'Bad decomposition: bad pattern frequency')
         end
      end
      --
      -- In this call-back, the counter of a pattern found during
      -- decomposition is decreased.
      local function cb_pdnm_pattern__decomposition_pattern_found(self, node, start)
         local pattern = self.trie:get_value(node)
--         print(pattern)
         -- Retrieve pattern counter.
         local cnt = self.expected[pattern]
         -- Detect extra patterns.
         assert(type(cnt) == 'number', 'Bad decomposition: extra pattern')
         -- Reduce counter.
         self.expected[pattern] = cnt - 1
      end
      --
      local o = class:new()
      mtest.read_hyphen_tex(o)
      -- Hook into call-back.
      o.cb_pdnm_pattern__decomposition_pattern_found = cb_pdnm_pattern__decomposition_pattern_found
      -- Run test set through tests.
      for s,expected in pairs(test_strings) do
         test(o, s, expected)
      end
   end,



}



-- Run test collection.
mtest.run_tests(TC)
