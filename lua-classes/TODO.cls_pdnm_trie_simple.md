# Make trie strictly work on string letters.

A simple trie can store words consisting of letters from an arbitrary
alphabet (a letter can not only be a character, but any Lua type).  This
has been introduced with applications outside text-processing in mind.
Since there is no such LuaTeX related application in sight, the code can
be made more specific so that letters can only be represented by
(UTF-8)-characters.

## Pros & cons

+ Code becomes less abstract.
+ And better maintainable.

- An abstract trie implementation may nevertheless be desirable.
  Maintain one within this code base for the time being?



# Make a decomposition trie object return all matching strings.

Currently, traversing a simple trie can only identify all stored strings
(keys) beginning with the same character(s) (prefix).  That is, when
keys `abcd`, `ce` are stored in a trie like this (asterisk denotes trie
root, row number relates to node level in trie, plus sign denotes end of
a key):

              *
          a      c
        b          e+
      c
    d+

Searching the string `abce` in this trie cannot identify key `ce`,
because that key can only be reached from the root node.  But when
character `c` occurs, we're not at the root node anymore, because
characters `a` and `b` already matched in the previous stages.  To find
all keys that may start at any position within a given string (what a
pattern trie does), any character has to advance search state from the
root node anew as well as from all other currently existing states.  A
list of "active tries" has to be managed, cf. function
`decomposition_advance()` in `cls_pdnm_pattern.lua`.

The problem is that the knowledge about another key starting with
character `c` can currently not be expoited at node c (at the left
branch).  It could be exploited by adding a fall-back edge at any node
pointing to the node representing the longest suffix of the current node
that can be reached from root.

In this example, fall-back edges at top level nodes a and c should point
to the root node, because a single character string has an empty suffix.
Fall-back edge at node b should point to root as well, this time because
there is no key starting with `b`, which is the only possible suffix of
`ab`.  Fall-back edge of left-branch node c should point to right-branch
node c, because `c` is the longest suffix of `abc` that can be reached
from root.  Now, when searching a string `abce` in this trie, at
(left-branch) node c, trying to match character `e` would not
immediately fail, but search would first turn via fall-back edge to
right-branch node c, where the search indeed succeeds.

This is in fact the [Aho–Corasick multi-string search
algorithm](https://en.wikipedia.org/wiki/Aho%E2%80%93Corasick_algorithm).

## Pros & cons

+ During trie traveral, less state management code is needed, keeping
  the executed code more compact (=> higher chance of not trashing CPU
  cache).  Additionally, as more invariant knowledge about search
  strings (patterns) can be exploited, many operations not contributing
  to the final solution can be saved early.  A string decomposition by
  Aho-Corasick algorithm should run significantly faster than the naive
  approach of managing a list of simple tries.

- Fall-back edges require more RAM per node.
- Computing fall-back nodes introduces a (one-)time penalty.

## Notes

* After inserting a key into a decomposition trie, all fall-back edges
  have to be re-computed.  So a trie should perhaps be frozen after
  populating it.  Searching a not-yet-frozen trie should error out.
* All keys reachable along normal edges (matching characters) start at
  the same position in the string in question.  When following a
  fall-back edge, that is not true anymore.  Following a fall-back edge
  needs to update start position in the string in question.  Correction
  value is current node depth minus suffix length (or the difference of
  start and end node level of a fall-back edge).
* Class inheritance: A decomposition trie should be derived from a
  simple trie.  A pattern class is then probably better derived from a
  decomposition trie instead of containing a trie as member variable.
  (Is a pattern class identical to a decomposition trie?  Probably.
  Don't forget to update file `README.classes`!)  Additionally, pattern
  class function `to_word()` is identical to simple trie class function
  `key()`.  Is that code duplication necessary?



# Cache decomposition results.

Decomposition result of one and the same string is constant.  In a text,
many words occur with a high frequency.  Decomposition results can be
cached in a table using the word string as key.

## Pros & cons

+ Reduces decomposition work.  Should improve run-time when the same
  keys have to be decomposed over and over again.

- When a key to decompose is not available as a string, but as a stream
  of characters, concatenation of all characters to generate a hash key
  may add additional load.  (Overall effect needs to be tested.)
- Caching requires RAM.

## Notes

This may affect applications of pattern and spot classes in that they
should already provide a key to decompose as a string, if possible.



<!--
%%% Local Variables: 
%%% coding: utf-8
%%% mode: markdown
%%% End: 
-->
