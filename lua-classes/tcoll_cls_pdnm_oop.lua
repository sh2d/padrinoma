-- -*- coding: utf-8 -*-



-- Load test module.
local mtest = require('mod_class_test')
-- Load class module to test.
local class = require('cls_pdnm_oop')



-- A sequence of all tests.  Test are provided as functions.
local TC = {



   --- Test class interface.
   -- A class instance is expected to inherit exactly the following
   -- (raw) interface from its proto-type class.
   [100] = function ()
      local o = class:new()
      mtest.test_prototype_interface(
         o,
         {
            --
            -- Private members.
            --
            -- Declared in module.
            init = true,
            new = true,
            --
            -- Public members.
            --
         }
      )
   end,



   --- Test class interface.
   -- A class instance is expected to provide exactly the following
   -- (raw) interface.
   [200] = function ()
      local o = class:new()
      mtest.test_instance_interface(
         o,
         {
            --
            -- Private members.
            --
            -- Initialized in constructor.
            super = true,
            --
            -- Public members.
            --
         }
      )
   end,



}



-- Run test collection.
mtest.run_tests(TC)
