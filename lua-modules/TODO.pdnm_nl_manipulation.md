# What is a word?

Applying hyphenation on a node list using a node list scanner and
regular TeX hyphenation patterns should ideally produce identical
results as TeX.  The notion of a word subject to hyphenation already
changed between TeX and LuaTeX.  Whereas in TeX a font change implies a
word boundary, in LuaTeX it does not (there is a code comment in file
`pdnm_nl_manipulation.lua` with some further information).  There are
other dark corners related to the question when to apply hyphenation to
a sequence of characters in TeX, e.g., a horizontal box next to a
sequence of letters inhibits hyphenation of that "word".

A node list scanner should identify words in a node list the way LuaTeX
does as much as possible.  But there's a catch: What has been said
before only applies to hyphenation of regular words.  There are
use-cases where the notion of a word should intentionally be different
to (Lua)TeX when applying pattern:

* Round-s replacement as well as ligature breaking should apply to words
  irrespective of a close horizontal box.
* In the German language, new nouns can be composed by combining
  existing nouns quite arbitrarily.  An author can optionally separate
  the components of a compound noun by a hyphen.  Traditional TeX never
  hyphenates words containing an explicit hyphen character.  In the
  German language, because of the sheer length of compound words,
  hyphenation within the components of a compound word is desirable,
  nevertheless.  In this case, the components of a compound word
  containing a hyphen should be hyphenated as if they are words on their
  own (possibly with a large hyphenmin value to prevent hyphenation near
  an explicit hyphen).
* Are there languages where some punctuation is considered part of a
  word (and the patterns)?
* What about bi-directional text?

There may be more cases requiring even different notions of a word.  To
cover different use-cases, different node list scanners applying
patterns to the different notions of a word are needed.  A user would
then have to choose the proper type of node list scanner for his
use-case.



# Apply multiple patterns in parallel.

To enable weighted hyphenation, e.g., prefering hyphenation at morpheme
boundaries over those within a stem or to discourage misleading
hyphenations, multiple pattern sets associated with different penalties
need to be applied to a word.  This can happen in parallel, so that
every letter of a word is only touched once.  The penalty introduced by
a potential hyphenation should finally depend on which pattern set did
or did not match at a letter position.  Currently, a node list scanner
can only apply exactly one pattern set to a word.

Note: This should not be too difficult to implement.

Dangerous bend: Linebreaking in TeX is guided by quite an awkward
calculation of demerits.  The overall effect of introducing different
penalties on paragraph justification quality is in no way intuitive.
Perhaps re-implement linebreaking in Lua with a more well-behaving
behaviour as an experimental playground?



# Apply pattern matching to node list instead of extracting words.

Currently, in a first stage a node list is scanned for all words, which
are saved as strings with associated data that enables a user to
point-back from a character to the correspondig node and its parent in
case of nested node lists, such as replacement lists in a discretionary
node.

Pattern matching could as well be applied to words in a node list
on-the-fly.  If a spot is found within a word, return start and end
nodes of the word.  That would simplify the API.  At the price of less
modularization (interweaving word identification and pattern matching).

Note, the information associated with the characters of a word (as far
as needed) could be stored in the node list itself via
[attributes](http://wiki.luatex.org/index.php/Attributes).



# Provide means to apply string or LPEG (re) patterns to a word instead of Liang patterns.

Alternative means to identify spots in a word could help in the
following use-cases:

* There may be languages where spots can reliably be identified via
  character-based rules.
* Even if less-then-ideal, to cover a language where no suitable
  patterns are available, character-based rules can fill the gap for the
  time being.



<!--
%%% Local Variables: 
%%% coding: utf-8
%%% mode: markdown
%%% End: 
-->
